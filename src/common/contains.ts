export const formatNumber = (num: number, between?: string) => {
  if (num === 0 || num === null) {
    return 0;
  }
  return num?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, between || ",");
};

export const formatDate = (date: Date, format?: string) => {
  const newDate = new Date(date);

  if (newDate) {
    const year = newDate?.getFullYear();
    const month = String(newDate?.getMonth() + 1).padStart(2, "0");
    const day = String(newDate?.getDate()).padStart(2, "0");
    const hour = String(newDate?.getHours()).padStart(2, "0");
    const minute = String(newDate?.getMinutes()).padStart(2, "0");

    const formattedDate = `${day}.${month}.${year} ${hour}:${minute}`;
    return formattedDate;
  }
};
