import { useEffect, useState } from "react";

import { Carousel } from "antd";
import { IMAGES } from "./assets/img/images";
import "./styles/global.scss";

import clsx from "clsx";
import LiveClockUpdate from "./components/LiveClockUpdate/LiveClockUpdate";
import PageOne from "./components/pages/PageOne";
import { getRootData } from "./components/pages/PageSlice";
import PageTwo from "./components/pages/PageTwo";
import socket from "./socket";
import { useAppDispatch } from "./store";
import PageThree from "./components/pages/PageThree";
import PageFour from "./components/pages/PageFour";
function App() {
  const dispatch = useAppDispatch();
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    socket.on("client", (data: any) => {
      if (data) {
        console.log("data", data);
        dispatch(getRootData(data));
      }
    });

    return () => {};
  }, [dispatch]);
  useEffect(() => {
    setTimeout(() => {
      setLoading(true);
    }, 1000 * 4);
  }, []);

  return (
    <div className="App">
      <section
        className={clsx("loader", {
          hidden: loading,
        })}
      >
        <p>l</p>
        <p>o</p>
        <p>a</p>
        <p>d</p>
        <p>i</p>
        <p>n</p>
        <p>g</p>
      </section>
      <div className="bg-[#131332] flex justify-between items-center px-[8%] ">
        <div>
          <img width={190} height={50} src={IMAGES.mainLogo} alt="main-logo" />
        </div>
        <div>
          <LiveClockUpdate />
        </div>
      </div>

      <Carousel
        dots={false}
        autoplay
        autoplaySpeed={1000 * 10}
        lazyLoad="progressive"
      >
        <PageTwo />
        <PageOne />
        <PageFour />
      </Carousel>
    </div>
  );
}

export default App;
