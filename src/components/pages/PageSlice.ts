import { AppThunkStatus } from "@/store";
import { createSlice } from "@reduxjs/toolkit";

interface AuthState {
  data?: any;
  status: AppThunkStatus;
  error: any;
  lastItem?: any;
  sameLastItem?: any;
  topItem?: any;
  sivImage: any;
}

const initialState: AuthState = {
  status: "idle",
  error: null,
  data: undefined,
  lastItem: undefined,
  sameLastItem: undefined,
  topItem: undefined,
  sivImage: undefined,
};

const pageSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    getRootData: (state, action) => {
      state.data = action.payload;
      if (Object.keys(action.payload.siv)) {
        state.lastItem =
          action.payload.siv[
            Object.keys(action.payload.siv).splice(
              Object.keys(action.payload.siv).length - 1,
              1
            )[0]!
          ];

        state.sivImage = action.payload.siv_images;

        state.topItem = Object.keys(state.lastItem)
          .slice(0, 4)
          .reduce((result: { [key: string]: string }, key) => {
            result[key] = state.lastItem[key];

            return result;
          }, {});

        state.sameLastItem =
          action.payload.siv[
            Object.keys(action.payload.siv).splice(
              Object.keys(action.payload.siv).length - 2,
              1
            )[0]!
          ];
      }
    },
  },
  extraReducers(builder) {},
});

export const { getRootData } = pageSlice.actions;

export default pageSlice.reducer;
