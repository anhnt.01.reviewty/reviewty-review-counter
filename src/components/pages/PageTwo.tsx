import { IMAGES } from "../../assets/img/images";
import { Col, Image, Row } from "antd";
import { formatNumber } from "../../common/contains";
import { useAppSelector } from "../../store";
import clsx from "clsx";

import styles from "./PageTwo.module.scss";
import { isNull } from "util";

const PageTwo = () => {
  const mainSlice = useAppSelector((slice) => slice.pageSlice);
  const { data: mainData } = mainSlice;
  console.log(mainData);

  return (
    <div className="bg-[#141332] w-full min-h-screen overflow-hidden text-[white] flex flex-col justify-start px-[8%]">
      <Row className="h-[545px]" gutter={[16, 16]}>
        <Col span={12} className="h-[50%]">
          <div
            style={{
              background: `url(${IMAGES.imgMainDisplay})`,
              height: "315px",
              backgroundRepeat: "no-repeat",
              backgroundSize: "cover",
              backgroundPosition: "center",
            }}
            className={clsx(
              "pt-4 h-[100%] relative text-center flex items-start border border-[#e9edf7] rounded-[2rem] ",
              styles.mainImg
            )}
          >
            <Row className="w-full">
              <Col span={12}>
                <p className="text-[1.125rem]">This Month Review</p>
                <p className="text-[#AD89FF] text-[0.875rem]">
                  {mainData?.time_period_this_month}
                </p>
                <p className="text-[4rem] font-bold">
                  {mainData?.this_month_total_review}
                </p>
              </Col>
              <Col span={12}>
                <p className="text-[1.125rem]">Predicted Number</p>
                <p className="text-[#BA92D9] text-[0.875rem]">
                  {mainData?.time_period_predicted}
                </p>
                <p className={clsx("text-[4rem] font-bold text-[#E3995F]", {})}>
                  {mainData?.predicted_monthly_review}
                </p>
                <Row className="justify-center">
                  <label className="flex bg-[#150D38] border border-[#3d1794] rounded-[5rem] px-4 py-2 mb-[2.25rem]">
                    <img
                      src={
                        mainData?.predicted_monthly_review.search("-") > 0
                          ? IMAGES.up
                          : IMAGES.down
                      }
                      alt=""
                    />
                    <span className="pl-1 text-[1.125rem]">
                      {mainData?.monthly_review_growth_rate}
                    </span>
                  </label>
                </Row>
              </Col>
              <Col span={24}>
                <div className="rounded-[2rem] w-[80%] m-auto bg-[rgba(255,255,255,0.25)] flex items-center justify-between px-[8%] -mt-[1rem]">
                  <div className="text-left">
                    <span className="text-[1.125rem]">New review</span>
                    <p className="text-[#AD89FF] text-[0.875rem]">
                      {mainData?.time_period_24h}
                    </p>
                  </div>

                  <span className="text-[2.25rem] font-bold">
                    {mainData?.new_review_vn}
                  </span>
                </div>
              </Col>
            </Row>
          </div>

          <div>
            <Row className="pt-4" gutter={[16, 0]}>
              {/* {renderItems.map((item) => ( */}
              <Col span={12}>
                <div
                  className="border border-[#e9edf7] rounded-[2rem]"
                  style={{
                    background: `url(${IMAGES.bgVN})`,
                    backgroundColor: "#2a2951",
                    height: "100%",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                  }}
                >
                  <div className="p-[10%] pb-0 flex flex-col items-center justify-between h-[100%]">
                    <div className="flex items-center flex-col">
                      <span className="capitalize text-[1.125rem] pl-2">
                        total review VN brand
                      </span>
                      <span className="capitalize pl-2 text-[#AD89FF]">
                        {mainData?.time_period_this_month}
                      </span>
                    </div>
                    <span className="text-[2.75rem] font-bold">
                      {mainData?.new_review}
                    </span>
                    <div className="flex  border border-[transparent] rounded-[5rem] px-4 py-2 mb-[2.25rem]">
                      {/* <img src={IMAGES.up} alt="" />
                      <span className="pl-1 text-[1.125rem]">3.21%</span> */}
                    </div>
                  </div>
                </div>
              </Col>
              <Col span={12}>
                <div
                  className="border border-[#e9edf7] rounded-[2rem]"
                  style={{
                    background: `url(${IMAGES.bgVNNew})`,
                    backgroundColor: "#2a2951",
                    height: "100%",
                    backgroundRepeat: "no-repeat",
                    backgroundSize: "cover",
                  }}
                >
                  <div className="p-[10%] pb-0 flex flex-col items-center justify-between h-[100%]">
                    <div className="flex items-center flex-col">
                      <span className="capitalize text-[1.125rem] pl-2">
                        new review VN brand
                      </span>
                      <span className="capitalize pl-2 text-[#AD89FF]">
                        {mainData?.time_period_24h}
                      </span>
                    </div>
                    <span className="text-[2.75rem] font-bold">
                      {mainData?.new_review_vn}
                    </span>
                    <div className="flex  border border-[transparent] rounded-[5rem] px-4 py-2 mb-[2.25rem]">
                      {/* <img src={IMAGES.up} alt="" />
                      <span className="pl-1 text-[1.125rem]">3.21%</span> */}
                    </div>
                  </div>
                </div>
              </Col>
              {/* ))} */}
            </Row>
          </div>
        </Col>

        <Col className="h-[200%] " span={12}>
          <div className="h-[548px]  overflow-y-scroll border border-[white] rounded-[2rem]">
            {mainData?.reviews?.[0] &&
              mainData?.reviews?.map(
                (
                  d: {
                    id: number;
                    content: string;
                    flag: string;
                    product: string;
                    user: string;
                  },
                  index: number
                ) => (
                  <div key={d.id}>
                    <div className="p-4 border border-b-[#767676]">
                      <p className="font-medium text-[1rem]">{d?.content}</p>
                      <p className="text-[#FFC525] flex py-1 items-center text-[1rem] font-medium">
                        <div className="pr-2" style={{ width: "56px" }}>
                          <Image
                            width={38}
                            src={require(`../../public/flags/${
                              typeof d?.flag === "string" ? d?.flag : "None"
                            }.png`)}
                          />
                        </div>
                        {d?.product}
                      </p>

                      <p className="text-[#BA92D9] text-[1rem] font-medium">
                        <span className="pr-2">
                          <Image src={IMAGES.user} alt="user" />
                        </span>
                        {d?.user}
                      </p>
                    </div>
                  </div>
                )
              )}
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default PageTwo;
