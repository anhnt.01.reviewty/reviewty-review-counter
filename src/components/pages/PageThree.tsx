import { useAppSelector } from "../../store";
import HighchartsReact from "highcharts-react-official";
import * as Highcharts from "highcharts";

import { useMemo } from "react";

const PageThree = () => {
  const mainSlice = useAppSelector((slice) => slice.pageSlice);
  const { lastItem, sameLastItem, topItem, data } = mainSlice;
  const options = useMemo(() => {
    return {
      title: {
        text: "Viewership Chart",
      },
      xAxis: {
        categories: Object.keys(lastItem),
        crosshair: true,
      },
      series: [
        {
          name: Object.keys(data?.siv)?.[0],
          type: "column",
          data: Object.keys(sameLastItem).map((key) => {
            return Number(sameLastItem[key]);
          }),
        },
        {
          name: Object.keys(data?.siv)?.[1],
          type: "column",
          data: Object.keys(lastItem).map((key) => {
            return Number(lastItem[key]);
          }),
        },
      ],
      credits: {
        enabled: false,
      },
    };
  }, [data?.siv, lastItem, sameLastItem]);

  if (!lastItem || !sameLastItem) {
    return <div></div>;
  } else {
    return (
      <div>
        {/* <HighchartsReact highcharts={Highcharts} options={options} /> */}
      </div>
    );
  }
};

export default PageThree;
