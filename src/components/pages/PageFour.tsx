// import { useAppSelector } from "@/store";
import { Image } from "antd";
import clsx from "clsx";
import { formatNumber } from "../../common/contains";
import { useAppSelector } from "../../store";

const PageFour = () => {
  const mainSlice = useAppSelector((slice) => slice.pageSlice);
  const { lastItem, sameLastItem, topItem, sivImage } = mainSlice;

  if (!lastItem || !sameLastItem) {
    return <div></div>;
  } else {
    return (
      <div className="bg-[#141332] w-full relative min-h-screen overflow-hidden text-[white] flex flex-col justify-start px-[8%]">
        <div className="grid grid-cols-4 gap-2 flex-col-reverse pb-2" dir="">
          {Object.keys(topItem).map((key: string, i: any) => {
            return (
              <div className="h-[130px] bg-[#FFFFFF] rounded-[8px]">
                <div className="mx-4  h-[40px] flex items-start justify-center rounded-b-[24px]">
                  <Image
                    style={{
                      objectFit: "contain",
                    }}
                    preview={false}
                    // className="h-[78px]"
                    src={require(`../../public/logo/${key}.png`)}
                    alt="brand"
                    width={100}
                    height={40}
                  />
                </div>

                {/* <div
                  style={{
                    width: "100%",
                    height: "30px",
                    background: `url(${sivImage[key]})`,
                    backgroundPosition: "center center",
                    backgroundSize: "cover",
                    backgroundRepeat: "no-repeat",
                    backgroundColor: "white",
                  }}
                ></div> */}

                <p
                  className={clsx(
                    "text-center px-4 text-[26px] font-bold flex items-center justify-center",
                    {
                      "text-[#cf2222]": lastItem[key] < sameLastItem[key],
                      "text-[#222]": lastItem[key] === sameLastItem[key],
                      "text-[#02B15A]": lastItem[key] > sameLastItem[key],
                    }
                  )}
                >
                  <span className="mr-2">
                    <Image src={sivImage[key]} height={80} width={120} />
                  </span>
                  {/* {formatNumber(lastItem[key])} */}
                </p>
                <p
                  className={clsx(
                    "text-center px-4 text-[14px] font-regular  rounded-[40px] w-[35%] flex items-center justify-center m-auto ",
                    {
                      "text-[#cf2222] bg-[#ffdada]":
                        lastItem[key] < sameLastItem[key],
                      "text-[#222] bg-[#c8c8c8]":
                        lastItem[key] === sameLastItem[key],
                      "text-[#02B15A] bg-[#02b15a26]":
                        lastItem[key] > sameLastItem[key],
                    }
                  )}
                >
                  {/* {(
                    ((lastItem[key] - sameLastItem[key]) / sameLastItem[key]) *
                    100
                  ).toFixed(1)}{" "}
                  % */}
                </p>
              </div>
            );
          })}
        </div>
        <div className="grid grid-cols-7 gap-2 flex-col-reverse" dir="">
          {Object.keys(lastItem).map((key: string, i: any) => {
            if (i < 4) {
              return <></>;
            }
            return (
              <div
                className="h-[130px] bg-[#FFFFFF] rounded-[8px]"
                // style={{
                //   background: `url(${sivImage[key]})`,
                //   backgroundPosition: "center",
                //   backgroundSize: "contain",
                //   backgroundRepeat: "no-repeat",
                //   backgroundColor: "white",
                // }}
              >
                <div className="mx-4  h-[56px] flex items-center justify-center rounded-b-[24px]">
                  <Image
                    style={{
                      objectFit: "contain",
                    }}
                    preview={false}
                    // className="h-[78px]"
                    src={require(`../../public/logo/${key}.png`)}
                    alt="brand"
                    width={100}
                    height={50}
                  />
                </div>

                <p
                  className={clsx(
                    "text-center px-4 text-[26px] font-bold flex items-center justify-center",
                    {
                      "text-[#cf2222]": lastItem[key] < sameLastItem[key],
                      "text-[#222]": lastItem[key] === sameLastItem[key],
                      "text-[#02B15A]": lastItem[key] > sameLastItem[key],
                    }
                  )}
                >
                  <span className="mr-2">
                    <Image src={sivImage[key]} height={70} width={120} />
                  </span>

                  {/* {formatNumber(lastItem[key])} */}
                </p>
                <p
                  className={clsx(
                    "text-center px-4 text-[14px] font-regular  rounded-[40px] w-[70%] flex items-center justify-center m-auto",
                    {
                      "text-[#cf2222] bg-[#ffdada99]":
                        lastItem[key] < sameLastItem[key],
                      "text-[#222] bg-[#c8c8c8]":
                        lastItem[key] === sameLastItem[key],
                      "text-[#02B15A] bg-[#02b15a26]":
                        lastItem[key] > sameLastItem[key],
                    }
                  )}
                >
                  {/* {(
                    ((lastItem[key] - sameLastItem[key]) / sameLastItem[key]) *
                    100
                  ).toFixed(1)}{" "} */}
                  {/* % */}
                </p>
              </div>
            );
            // if (i === 15) return <div key={i}></div>;
            // return (
            //   <motion.div
            //     key={i}
            //     style={{
            //       display: "flex",
            //       flexDirection: "column",
            //       alignItems: "center",
            //       justifyContent: "center",
            //       width: 200,
            //       height: 200,
            //       borderRadius: "100%",
            //       backgroundColor: "#fd3",
            //       position: "absolute",
            //       left: position[i]?.left,
            //       top: position[i]?.top,
            //     }}
            //     animate={{
            //       x: [0, 100, 100, 0, 0],
            //       y: [0, 0, 100, 100, 0],
            //       backgroundColor: ["#FFE7E7"],
            //     }}
            //     transition={{
            //       duration: 12,
            //       ease: "linear",
            //       repeat: Infinity,
            //       delay: i + 1,
            //     }}
            //   >
            //     <img
            //       src={require(`../../public/logo/${k}.png`)}
            //       alt="brand"
            //       width={100}
            //     />
            //     {/* <p className="text-[red]">{k}</p> */}
            //     <p className="text-[28px] font-bold text-[#F08F8C]">
            //       {getNewestData[k]}
            //     </p>
            //   </motion.div>
            // );
          })}
        </div>
      </div>
    );
  }
};

export default PageFour;
