import React, { useEffect, useState } from "react";
import moment from "moment";

let thisTimer: string | number | NodeJS.Timer | undefined;
const LiveClockUpdate = () => {
  const [date, setDate] = useState(new Date());

  useEffect(() => {
    thisTimer = setInterval(() => {
      tick();
    }, 1000);

    return () => {
      clearInterval(thisTimer);
    };
  }, []);

  const tick = () => {
    setDate(new Date());
  };

  return (
    <div className="bg-[transparent] flex items-end">
      <h2 className="text-white bg-[transparent] text-[56px] font-bold">
        {/* {date.toLocaleTimeString()}. */}
        {moment(date).format("HH:mm:ss")}
      </h2>
      <span className="text-[2rem] text-white font-normal pb-2 pl-6">
        {moment(date).format("DD/MM/YYYY")}
      </span>
    </div>
  );
};

export default LiveClockUpdate;
