// import imgMainDisplay from "../../public/bg-main-display.png";
import imgMainDisplay from "../../public/bg-main-display.png";
import bg1 from "../../public/bg.png";
import bg2 from "../../public/bg2.png";
import bg3 from "../../public/bg3.png";
import bg4 from "../../public/bg4.png";
import mainLogo from "../../public/logo.png";
import iconQuick from "../../public/icon-quick.svg";
import iconQuickNew from "../../public/icon-quick-new.svg";
import iconVN from "../../public/icon-vn.svg";
import iconVNNew from "../../public/icon-vn-new.svg";
import up from "../../public/up.svg";
import down from "../../public/down.svg";
import bgVNNew from "../../public/Bg-VN-New.png";
import bgVN from "../../public/Bg-VN.png";
import user from "../../public/user.svg";
// import peacholic from "../../public/peacholic.png";
export const IMAGES = {
  // peacholic,
  user,
  imgMainDisplay,
  bg1,
  bg2,
  bg3,
  bg4,
  mainLogo,
  iconQuick,
  iconQuickNew,
  iconVN,
  iconVNNew,
  up,
  down,
  bgVN,
  bgVNNew,
};
